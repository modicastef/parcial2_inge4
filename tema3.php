<?php
define("fila", 3);
define("columna", 3);

function generarMatrizCuadrada() {
    $matriz = array();

    for ($i = 0; $i < fila; $i++) {
        $fila = array();

        for ($j = 0; $j < columna; $j++) {
            $fila[] = rand(0, 9);
        }

        $matriz[] = $fila;
    }

    return $matriz;
}

function sumaDiagonal($matriz) {
    $suma = 0;

    for ($i = 0; $i < fila; $i++) {
        for ($j = 0; $j < columna; $j++) {
            if ($i == $j) {
                $suma += $matriz[$i][$j];
            }
        }
    }

    return $suma;
}

function dibujarMatrizCuadrada($matriz) {
    echo "Matriz generada:\n";
    echo "<table border='3'>";

    for ($i = 0; $i < fila; $i++) {
        echo "<tr>";

        for ($j = 0; $j < columna; $j++) {
            echo "<td>" . $matriz[$i][$j] . "</td>";
        }

        echo "</tr>";
    }

    echo "</table>";
}

function main() {
    $matriz = generarMatrizCuadrada();
    dibujarMatrizCuadrada($matriz);
    echo "<br>";

    $suma = sumaDiagonal($matriz);

    if ($suma >= 10 && $suma <= 15) {
        echo "El valor de la diagonal principal es: {$suma}. FIN";
        exit;
    } else {
        main();
    }
}

main();
?>